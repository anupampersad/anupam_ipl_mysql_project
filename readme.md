## <u>Create IPL Database</u>
To create the database, comment out the `database` key in `createConnection` function.

```javascript
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    // database: 'IPL'
});
```
Run the application using `npm start`

go to [http://localhost:3000/createdb]()

The database will get created

Now Uncomment the `database` key
```javascript
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'IPL'
});
```
<br>

## <u>Create Matches Table</u>
Start the application using `npm start`

In Browser, go to [http://localhost:3000/creatematchestable](http://localhost:3000/creatematchestable)
<br>
<br>


## <u>Create Deliveries Table</u>
Start the application using `npm start`

In Browser, go to  [http://localhost:3000/createdeliveriestable](http://localhost:3000/createdeliveriestable)
<br>
<br>


## <u>Insert data into matches table</u>
Start the application using `npm start`

In Browser, go to [http://localhost:3000/insertdataintomatches](http://localhost:3000/insertdataintomatches)
<br>
<br>

## <u>Insert data into matches table</u>
Start the application using `npm start`

In Browser, go to [http://localhost:3000/insertdataintodeliveries](http://localhost:3000/insertdataintodeliveries)






