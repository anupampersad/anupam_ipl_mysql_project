const express = require('express');
const app = express()
const mysql = require('mysql');
const fs = require('fs')

// Create connection
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'IPL'
});

// Connect
db.connect((err) => {
    if (err) {
        console.log(err)
    } else {
        console.log('MySql Connected...');
    }
});

// Create IPL Database
app.get('/createdb', (req, res) => {
    let sql = 'CREATE DATABASE IPL';
    db.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);
        res.send('IPL Database created...');
    });
});

// Create Matches Table in database
app.get('/creatematchestable', (req, res) => {

    fs.readFile('./createTable/CreateMatchesTable.sql', 'utf-8', (error, data) => {

        if (error) {
            console.log(error)
        }
        else {

            let sql = `${data}`
            db.query(sql, (err, result) => {
                if (err) throw err;
                else {
                    console.log('Matches table created...');
                    res.send('Matches table created...');
                }

            });

        }
    })
});

// Create Deliveries Table in database
app.get('/createdeliveriestable', (req, res) => {

    fs.readFile('./createTable/CreateDeliveriesTable.sql', 'utf-8', (error, data) => {

        if (error) {
            console.log(error)
        }
        else {

            let sql = `${data}`
            db.query(sql, (err, result) => {
                if (err) throw err;
                console.log('Deliveries table created...');
                res.send('Deliveries table created...');
            });

        }
    })
});

// Insert data into matches table form matches.csv file
app.get('/insertdataintomatches', (req, res) => {

    fs.readFile('./data/matches.csv', 'utf-8', async (error, data) => {

        if (error) {
            console.log(error)
        }
        else {
            let matches_data = data.split('\n')
            let rows = matches_data.reduce((acc, row) => {
                let match = row.split(',')

                // remove '\r'  
                match.pop()

                // Add an empty column if last umpire is not provided because comma is not provided after last value in csv file
                if (match.length == 17) {
                    match.push(' ')
                }

                acc.push(match)
                return acc
            }, [])

            // Remove CSV header
            rows.shift()

            var sql = "INSERT INTO matches VALUES ?";
            db.query(sql, [rows], function (err, result) {
                if (err) throw err;
                console.log("Number of records inserted in matches table : " + result.affectedRows);
                res.send("Number of records inserted in matches table : " + result.affectedRows)
            });

        }
    })

})

// Insert data into deliveries table from deliveries.csv file
app.get('/insertdataintodeliveries', (req, res) => {
    fs.readFile('./data/deliveries.csv', 'utf-8', async (error, data) => {

        if (error) {
            console.log(error)
        }
        else {
            let deliveries_data = data.split('\n')
            let rows = deliveries_data.reduce((acc, row) => {
                let delivery = row.split(',')

                // remove '\r'  
                delivery.pop()

                // Add an empty column if last column fielder is not provided because comma is not provided after last value in csv file
                if (delivery.length == 20) {
                    delivery.push(' ')
                }

                // console.log(delivery.length)
                acc.push(delivery)
                return acc
            }, [])

            // Remove CSV header
            rows.shift()

            var sql = "INSERT INTO deliveries VALUES ?";
            db.query(sql, [rows], function (err, result) {
                if (err) throw err;
                console.log("Number of records inserted in deliveries table : " + result.affectedRows);
                res.send("Number of records inserted in deliveries table : " + result.affectedRows)
            });
        }
    });
})

// Fetch matches per year
app.get('/matchesPerYear', (req, res) => {
    let sql = 'Select season,count(season) AS Matches FROM matches GROUP BY season ORDER BY season';
    let query = db.query(sql, (err, results) => {
        if (err) throw err;
        res.send(results);
    });
});

//Fetch matches won per team per year
app.get('/matchesWonPerTeamPerYear', (req, res) => {
    let sql = 'SELECT winner,season,COUNT(winner) AS matches_won FROM matches GROUP BY winner,season ORDER BY winner,season;';
    let query = db.query(sql, (err, results) => {
        if (err) throw err;
        res.send(results);
    });
})

// Fetch top 10 Economic bowlers from year 2015
app.get('/topEcoBowlers2015', (req, res) => {
    let sql = 'SELECT bowler, SUM(total_runs) TOTAL_RUNS, COUNT(ball)/6 AS OVERS, SUM(total_runs)/(Count(ball)/6) AS ECONOMY FROM matches JOIN deliveries ON matches.id = deliveries.match_id WHERE season=2015 GROUP BY bowler ORDER BY ECONOMY LIMIT 10;';
    let query = db.query(sql, (err, results) => {
        if (err) throw err;
        res.send(results);
    });
})

// Fetch extra runs conceded per team in year 2016
app.get('/extraRunsConcededPerTeam2016', (req, res) => {
    let sql = 'SELECT bowling_team as Team,SUM(extra_runs) as extra_runs_conceded FROM matches JOIN deliveries ON matches.id = deliveries.match_id where season=2016 GROUP BY bowling_team;';
    let query = db.query(sql, (err, results) => {
        if (err) throw err;
        res.send(results);
        console.log(results)
    });
})

app.listen('3000', () => {
    console.log('Server started on port 3000');
});