-- Matches Per Year
Select season,count(season) 
FROM matches 
GROUP BY season 
ORDER BY season;

-- Extra runs conceded per team in year 2016
SELECT season,bowling_team,SUM(extra_runs) as EXTRA_RUNS_CONCEDED 
FROM matches 
JOIN deliveries ON matches.id = deliveries.match_id 
where season=2016 
GROUP BY bowling_team;

-- Top 10 economical bowlers in the year 2015
SELECT bowler, 
SUM(total_runs) TOTAL_RUNS, 
COUNT(ball)/6 AS OVERS, 
SUM(total_runs)/(Count(ball)/6) AS ECONOMY 
FROM matches JOIN deliveries 
ON matches.id = deliveries.match_id 
WHERE season=2015 
GROUP BY bowler 
ORDER BY ECONOMY 
LIMIT 10;

-- Matches Won Per Team Per Year
SELECT winner,season,COUNT(winner) FROM matches GROUP BY winner,season ORDER BY winner,season;