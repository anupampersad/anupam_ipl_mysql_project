create table deliveries (
	match_id INT ,
	inning INT,
	batting_team VARCHAR(50),
	bowling_team VARCHAR(50),
	overs INT, -- over is a reserved keyword in MySQL and hence I have overs instead of over
	ball INT,
	batsman VARCHAR(50),
	non_striker VARCHAR(50),
	bowler VARCHAR(50),
	is_super_over BOOLEAN,
	wide_runs INT,
	bye_runs INT,
	legbye_runs INT,
	noball_runs INT,
	penalty_runs INT,
	batsman_runs INT,
	extra_runs INT,
	total_runs INT,
	player_dismissed VARCHAR(50),
	dismissal_kind VARCHAR(50),
	fielder VARCHAR(50),
	FOREIGN KEY (match_id) REFERENCES matches(id)
);